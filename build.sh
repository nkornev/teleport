#!/bin/bash
env GOOS=windows GOARCH=amd64 go build -o build/windows/teleport.exe .
env GOOS=linux GOARCH=amd64 go build -o build/linux/teleport .
env GOOS=darwin GOARCH=amd64 go build -o build/osx/teleport .