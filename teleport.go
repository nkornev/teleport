package main

import (
	"database/sql"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"runtime"
	"strings"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	_ "github.com/go-sql-driver/mysql"
	"github.com/joho/godotenv"
	"github.com/schollz/progressbar"
)

const dbType = "mysql"

var (
	db        DbParams
	sqlTable  string
	sqlColumn string
	dbConn    *sql.DB
	s3Client  *s3.S3
	doSpace   string
	doUri     string
	doCdnUrl  string
	doRegion  string
	filesPath string
	urlPrefix string
)

func main() {
	fmt.Println("TELEPORT:", "starting...")

	dbConn = getDbConnection()
	defer func(dbConn *sql.DB) {
		err := dbConn.Close()
		if err != nil {
			panic(err)
		}
	}(dbConn)

	processRecords()
}

func init() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	filesPath = os.Getenv("FILES_PATH")
	if len(filesPath) == 0 {
		log.Fatal("No path to files provided")
	}

	urlPrefix = os.Getenv("URL_PREFIX")
	if len(urlPrefix) == 0 {
		log.Fatal("No URL prefix provided")
	}

	initS3Client()

	initDb()
}

func initS3Client() {
	doKey := os.Getenv("DO_ACCESS_KEY")
	if len(doKey) == 0 {
		log.Fatal("No DO access key provided")
	}

	doSecret := os.Getenv("DO_SECRET_KEY")
	if len(doSecret) == 0 {
		log.Fatal("No DO secret key provided")
	}

	doUrl := os.Getenv("DO_URL")
	if len(doUrl) == 0 {
		log.Fatal("No DO URL provided")
	}

	doRegion = os.Getenv("DO_REGION")
	if len(doRegion) == 0 {
		log.Fatal("No DO region provided")
	}

	doSpace = os.Getenv("DO_SPACE")
	if len(doSpace) == 0 {
		log.Fatal("No DO space provided")
	}

	doUri = os.Getenv("DO_URI")

	doCdnUrl = os.Getenv("DO_CDN_URL")
	if len(doCdnUrl) == 0 {
		log.Fatal("No DO CDN URL provided")
	}

	s3Config := &aws.Config{
		Credentials: credentials.NewStaticCredentials(doKey, doSecret, ""),
		Endpoint:    aws.String(fmt.Sprintf(doUrl, doRegion)),
		Region:      aws.String(doRegion),
	}

	s3Session, err := session.NewSession(s3Config)
	if err != nil {
		log.Fatal("Error creating S3 session")
	}

	s3Client = s3.New(s3Session)
}

func initDb() {
	db = DbParams{}

	db.Host = os.Getenv("SQL_HOST")
	if len(db.Host) == 0 {
		log.Fatal("No SQL host provided")
	}

	db.Port = os.Getenv("SQL_PORT")
	if len(db.Port) == 0 {
		log.Fatal("No SQL port provided")
	}

	db.User = os.Getenv("SQL_USER")
	if len(db.User) == 0 {
		log.Fatal("No SQL user provided")
	}

	db.Pass = os.Getenv("SQL_PASS")
	if len(db.Pass) == 0 {
		log.Fatal("No SQL password provided")
	}

	db.Db = os.Getenv("SQL_DB")
	if len(db.Db) == 0 {
		log.Fatal("No SQL database name provided")
	}

	sqlTable = os.Getenv("SQL_TABLE")
	if len(sqlTable) == 0 {
		log.Fatal("No SQL table name provided")
	}

	sqlColumn = os.Getenv("SQL_COLUMN")
	if len(sqlColumn) == 0 {
		log.Fatal("No SQL column name provided")
	}
}

func getDbConnection() *sql.DB {
	db, err := sql.Open(dbType, fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", db.User, db.Pass, db.Host, db.Port, db.Db))

	if err != nil {
		log.Fatal("Failed to connect to the database")
	}

	db.SetConnMaxLifetime(time.Minute * 3)
	db.SetMaxOpenConns(10)
	db.SetMaxIdleConns(10)

	return db
}

func processRecords() {
	var count int64
	_ = dbConn.QueryRow(fmt.Sprintf("SELECT COUNT(*) FROM %s.%s", db.Db, sqlTable)).Scan(&count)

	bar := progressbar.Default(count)

	res, err := dbConn.Query(fmt.Sprintf("SELECT id, %s FROM %s.%s", sqlColumn, db.Db, sqlTable))
	if err != nil {
		panic(err)
	}

	defer func(res *sql.Rows) {
		err := res.Close()
		if err != nil {
			panic(err)
		}
	}(res)

	counter := 0
	for res.Next() {
		counter++

		var record Record
		err := res.Scan(&record.id, &record.link)

		if err != nil {
			panic(err)
		}

		err = processRecord(record)
		if err != nil {
			fmt.Println("\n" + err.Error())
		}

		if counter%5 == 0 {
			runtime.GC()
		}

		err = bar.Add(1)
		if err != nil {
			return
		}
	}
}

func processRecord(record Record) error {
	fileName := strings.TrimPrefix(record.link, urlPrefix)
	url, err := uploadFile(fileName)
	if err != nil {
		return err
	}

	err = updateRecord(record.id, url)
	if err != nil {
		return err
	}

	return nil
}

func updateRecord(id int, url string) error {
	sqlQuery := fmt.Sprintf("UPDATE %s SET %s = ? WHERE id = ?", sqlTable, sqlColumn)

	stmt, err := dbConn.Prepare(sqlQuery)
	if err != nil {
		return err
	}

	defer func(stmt *sql.Stmt) {
		err := stmt.Close()
		if err != nil {
			panic(err)
		}
	}(stmt)

	_, err = stmt.Exec(url, id)
	if err != nil {
		return errors.New("Failed to update URL for record: " + string(rune(id)) + " ==>> " + err.Error())
	}

	return nil
}

func uploadFile(fileName string) (string, error) {
	filePath, err := getFilePath(fileName)
	if err != nil {
		return "", err
	}

	fileContent, err := ioutil.ReadFile(filePath)
	if err != nil {
		return "", errors.New("Failed to read file content - " + filePath)
	}

	if len(doUri) > 0 {
		fileName = doUri + "/" + fileName
	}

	object := s3.PutObjectInput{
		Bucket: aws.String(doSpace),
		Key:    aws.String(fileName),
		Body:   strings.NewReader(string(fileContent)),
		ACL:    aws.String("public-read"),
	}

	_, err = s3Client.PutObject(&object)
	if err != nil {
		return "", err
	}

	return fmt.Sprintf(doCdnUrl, doSpace, doRegion, fileName), nil
}

func getFilePath(fileName string) (string, error) {
	path := filesPath + "/" + fileName

	_, err := os.Stat(path)
	if err != nil {
		if os.IsNotExist(err) {
			return "", errors.New("File not found: " + path)
		}
	}

	return path, nil
}
