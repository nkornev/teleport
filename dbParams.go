package main

type DbParams struct {
	Host string
	Port string
	User string
	Pass string
	Db   string
}
